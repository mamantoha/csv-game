extern crate csv;
extern crate memmap;

use std::env;
use memmap::{Mmap, Protection};

fn main() {
    let fpath = env::args_os().nth(1).unwrap();
    let mmap = match Mmap::open_path(fpath, Protection::Read) {
        Ok(mmap) => mmap,
        Err(_) => { println!("0"); return; }
    };
    let data = unsafe { mmap.as_slice() };

    let mut count = 0;
    let mut rdr = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(data);
    let mut record = csv::ByteRecord::new();
    while rdr.read_byte_record(&mut record).unwrap() {
        count += record.len();
    }
    println!("{}", count);
}
